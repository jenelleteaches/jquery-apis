## Bitcoin Currency Converter app

This app converts Bitcoins to your local currency.

1. Enter the amount of Bitcoins
2. Enter the local currency
3. Press convert
4. Done!

## API

This app uses the [CoinDesk.com API](https://www.coindesk.com/api) to get the current exchange rate for Bitcoins.

## Screenshot

![](screenshots/app.png)
