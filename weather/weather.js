
let API_KEY = "fcfaf8ac620fe5a836713b9a60cc2805"

$( document ).ready(function() {
    console.log("ready!");

    // ------------------------
    // When the page loads, get weather.
    // Darksky.net is annoying - they block cross orgin request.
    // Therefore, you have do a different hack to get the API request to work.
    let url = "https://api.darksky.net/forecast/ab7e37126fbeddc41405bc6e414058fe/43.761539,-79.411079"

    // To "hack" the system, use $.ajax instead of $.get
    // Its pretty much the same thing, except $.ajax gives you more control over the request.
    // (and you can configure options)
    $.ajax({
        // tell browser what URL to visit
        url: url,
        // specify if you will be doing a GET or POST
        type:"GET",
        // This is the secret hack that makes the API work properly
        dataType: "jsonp",

        // This is what you should do when your browser
        // receives a response from Darksky.net
        success: function(data, code) {
            console.log("got the data response")
            console.log(data); // server response

            // Extract the "temperature" and "weather conditions" value from the dictionary.
            let rate = data["currently"]["summary"]
            let tempF = data["currently"]["temperature"]
            tempF = parseFloat(tempF)

            // convert temp to case expression:
            tempC = (tempF - 32) * 5/9
            var h = tempC.toFixed(0) + "&#730;C"
            $("#currentTemperature").html(h)

        }

    });
})
