let exchangeRate = 0;
let currencyCode = "CAD"

$( document ).ready(function() {
    console.log("ready!");

    // ------------------------
    // Add click handlers for the buttons on the page
    $("#changeCurrencyLink").click(showCurrencyDiv);
    $("#changeCurrencyButton").click(changeCurrencyPressed)

    // ------------------------
    // When the page loads, get the current exchange rate in the default currency (CAD)
    let url = "https://api.coindesk.com/v1/bpi/currentprice/CAD.json"
    $.get(url, function( data ) {
      console.log(data);
      // 1. Convert the reponse data to something useful
      // This api returns the data as a string.
      // Thus you must convert it to a JSON dictionary before you can use it.
      var jsonData = JSON.parse(data);

      // 2. Extract the "rate" value from the dictionary.
      // Rate is located in data -> bpi key -> CAD key --> rate key
      // Notice that the exchange rate comes in the "rate" key and the "rate_float" key.
      // I chose "rateFloat" because it looks like a number, not a human readable strings
      // eg:  5000 vs. 5,000
      let rate = jsonData["bpi"]["CAD"]["rate_float"]

      // setting the global exchange rate value
      exchangeRate = parseFloat(rate);

      // 3. Show the exchange rate in the DOM

      // Instead of doing this:
      //  let p = document.createElement("p");
      //  p.innerHTML = "Today's rate is: " + exchangeRate;
      //  document.getElementById("#todaysRate").appendChild(p)

      // You do this:

      var htmlToAdd = "<p>Today's rate is: " + exchangeRate + "</p>"
      $("#todaysRate").append(htmlToAdd);

    });   // end get

});   // end document.ready()


// Click handler for when user clicks CONVERT buttons
// Get the amount and convert to the local currency
function convertButtonClicked() {
  console.log("convert button clicked" + exchangeRate);

  // 1 . Get amount user entered & convert to float
  let amount = $("#userInputBox").val()
  amount = parseFloat(amount)

  // 2.  Do the math
  let result = amount * exchangeRate;

  // 3. Output it to the DOM
  var output = "<p>" + amount + " BTC is " + result.toFixed(2) + " " + currencyCode;
  $("#results").html(output);
}

// Show and hide the CHANGE CURRENCY div
function showCurrencyDiv() {
  console.log("showing the change currency div");
  $("#selectCurrency").slideToggle();
}


// Click handler function for when user presses CHANGE CURRENCY button
function changeCurrencyPressed() {
  console.log("user wants to change currency!");
  var newCurrency = $("#newCurrencyBox").val();
  // 1. Change the global currency
  currencyCode = newCurrency;

  // 2. Also update the currency code in the UI
  $("#currencyCode").html(newCurrency)

  // 3. Get the exchange rate from the API
  let url = "https://api.coindesk.com/v1/bpi/currentprice/" + newCurrency + ".json"

  console.log("url: " + url)

  $.get(url, function( data ) {
    // This is a repeat of the $.get function code in the $document.ready() function
    var jsonData = JSON.parse(data);
    let rate = jsonData["bpi"][newCurrency]["rate_float"]
    console.log("New Rate: " + rate);
    exchangeRate = parseFloat(rate);

    var htmlToAdd = "<p>Today's rate is: " + exchangeRate + "</p>"

    // Delete the previous contents of the <todayRate> div
    // with this new exchange rate data
    // This is the same as doing ___.innerHTML = htmlToAdd
    $("#todaysRate").html(htmlToAdd);
  });
}
